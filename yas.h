/*
 * Copyright (c) 2010-2012 Yamaha Corporation
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef __YAS_H__
#define __YAS_H__

#include "yas_cfg.h"

#define YAS_VERSION	"7.0.2a"
#define DMT_DEBUG_DATA
#define GSE_TAG                  "[DMT_Gsensor]"
#ifdef DMT_DEBUG_DATA
#define GSE_ERR(fmt, args...)    printk(KERN_ERR GSE_TAG"%s %d : "fmt, __FUNCTION__, __LINE__, ##args)
#define GSE_LOG(fmt, args...)    printk(KERN_INFO GSE_TAG fmt, ##args)
#define GSE_FUN(f)               printk(KERN_INFO GSE_TAG" %s: %s: %i\n", __FILE__, __func__, __LINE__)
#define DMT_DATA(dev, ...)		 dev_dbg((dev), ##__VA_ARGS__)
#else
#define GSE_ERR(fmt, args...)
#define GSE_LOG(fmt, args...)
#define GSE_FUN(f)
#define DMT_DATA(dev, format, ...)
#endif


/* ----------------------------------------------------------------------------
 *                             Typedef definition
 *--------------------------------------------------------------------------- */

#if defined(__KERNEL__)
#include <linux/types.h>
#else
#include <stdint.h>
/*typedef signed char	int8_t;*/
/*typedef unsigned char	uint8_t;*/
/*typedef signed short	int16_t;*/
/*typedef unsigned short	uint16_t;*/
/*typedef signed int	int32_t;*/
/*typedef unsigned int	uint32_t;*/
#endif

/* ----------------------------------------------------------------------------
 *                              Macro definition
 *--------------------------------------------------------------------------- */

/* Debugging */
#define DEBUG	(1)

#define YAS_REPORT_DATA				(0x01)
#define YAS_REPORT_CALIB			(0x02)
#define YAS_REPORT_OVERFLOW_OCCURED		(0x04)
#define YAS_REPORT_HARD_OFFSET_CHANGED		(0x08)
#define YAS_REPORT_CALIB_OFFSET_CHANGED		(0x10)

#define YAS_HARD_OFFSET_UNKNOWN			(0x7f)
#define YAS_CALIB_OFFSET_UNKNOWN		(0x7fffffff)

#define YAS_NO_ERROR				(0)
#define YAS_ERROR_ARG				(-1)
#define YAS_ERROR_NOT_INITIALIZED		(-2)
#define YAS_ERROR_BUSY				(-3)
#define YAS_ERROR_DEVICE_COMMUNICATION		(-4)
#define YAS_ERROR_CHIP_ID			(-5)
#define YAS_ERROR_NOT_ACTIVE			(-6)
#define YAS_ERROR_RESTARTSYS			(-7)
#define YAS_ERROR_HARDOFFSET_NOT_WRITTEN	(-8)
#define YAS_ERROR_INTERRUPT			(-9)
#define YAS_ERROR_ERROR				(-128)

#ifndef NULL
#define NULL ((void *)(0))
#endif
#ifndef FALSE
#define FALSE (0)
#endif
#ifndef TRUE
#define TRUE (!(0))
#endif
#ifndef NELEMS
#define NELEMS(a) ((int)(sizeof(a)/sizeof(a[0])))
#endif
#ifndef ABS
#define ABS(a) ((a) > 0 ? (a) : -(a))
#endif
#ifndef M_PI
#define M_PI (3.14159265358979323846)
#endif

/* ----------------------------------------------------------------------------
 *                            Structure definition
 *--------------------------------------------------------------------------- */

/*
struct yas_vector {
	int32_t v[3];
};*/
#define SENSOR_DATA_SIZE 		3 
typedef union {
	struct {
		int32_t	x;
		int32_t	y;
		int32_t	z;
	} u;
	int32_t	v[SENSOR_DATA_SIZE];
} raw_data;



struct yas_acc_data {
	raw_data xyz;
	raw_data raw;
};

struct yas_acc_filter {
	int threshold; /* um/s^2 */
};

struct yas_acc_driver_callback {
	int (*lock)(void);
	int (*unlock)(void);
	int (*device_open)(void);
	int (*device_close)(void);
	int (*device_write)(uint8_t adr, const uint8_t *buf, int len);
	int (*device_read) (uint8_t adr, uint8_t *buf, int len);
	void (*msleep)(int msec);
};

struct yas_acc_driver {
	int (*init)(void);
	int (*term)(void);
	int (*get_delay)(void);
	int (*set_delay)(int delay);
	int (*get_offset)(struct raw_data *offset);
	int (*set_offset)(struct raw_data *offset);
	int (*get_enable)(void);
	int (*set_enable)(int enable);
	int (*get_filter)(struct yas_acc_filter *filter);
	int (*set_filter)(struct yas_acc_filter *filter);
	int (*get_filter_enable)(void);
	int (*set_filter_enable)(int enable);
	int (*get_position)(void);
	int (*set_position)(int position);
	int (*measure)(struct yas_acc_data *data);
#if DEBUG
	int (*get_register)(uint8_t adr, uint8_t *val);
#endif
	struct yas_acc_driver_callback callback;
};

struct yas_acc_calibration_threshold {
	int32_t variation;
};

struct yas_acc_calibration_callback {
	int (*lock)(void);
	int (*unlock)(void);
};

struct yas_acc_calibration {
	int (*init)(void);
	int (*term)(void);
	int (*update)(struct raw_data *acc);
	int (*get_offset)(struct raw_data *offset);
	int (*get_threshold)(struct yas_acc_calibration_threshold *threshold);
	int (*set_threshold)(struct yas_acc_calibration_threshold *threshold);
	struct yas_acc_calibration_callback callback;
};


/* ----------------------------------------------------------------------------
 *                         Global function definition
 *--------------------------------------------------------------------------- */


int yas_acc_driver_init(struct yas_acc_driver *f);
int yas_acc_calibration_init(struct yas_acc_calibration *f);


#endif /* __YAS_H__ */
